![badge](https://img.shields.io/badge/Estado-En%20proceso-blue.svg)


# Presentación de la práctica

Se ha realizado una maquetación web de un portal de reserva de restaurantes. **El ámbito de esta práctica es grupal**. El equipo formado ha decidido englobarse bajo el nombre de *The Middle Table*.

<img src="/img/logo-tmt.png" alt="logo-middle-table" height="100"/>
<img src="/img/green-table-logo.png" alt="logo-green-table" height="100"/>

## ¿Qué es Green Table?

*Green Table* es el nombre de la plataforma web que desde *The Middle Table* hemos querido desarrollar. Se basa en una plataforma de reserva de mesas en restaurantes, con la particularidad de que los restaurantes ofertados son solo aquellos que ofrecen menús vegetarianos, de ahí el nombre de *Green Table*. 
La web está inspirada en el famoso portal de reservas de mesa online [OpenTable](https://www.opentable.com/).


## Proceso de desarrollo

Hemos trabajado bajo un márco de trabajo ágil, aplicando algunos de los eventos, artefactos y roles de *Scrum*. Como herramientas de desarrollo hemos utilizado *Trello, Git, Photoshop y Visual Code Studio*.

Adicionalmente, con la finalidad de realizar un trabajo coherente entre las diferentes partes del equipo, utilizamos un pequeño mockup para poder tener en mente una estructura común.